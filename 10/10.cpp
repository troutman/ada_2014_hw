#include<cstdio>
#include<cstdlib>


struct Kuos
{
	int iq;
	int ask_i;
};

void find_smarter(Kuos* K, int j)
{
	int tmp = j-1;
	if(K[tmp].iq > K[j].iq)
		K[j].ask_i = tmp;
	else{
		while(1){
			if(K[K[tmp].ask_i].iq <= K[j].iq){
				tmp = K[tmp].ask_i;
			}
			else{
				K[j].ask_i = K[tmp].ask_i;
				break;
			}			
		}
	}
	return;
}

int main()
{
	int T=0,n=0;
	Kuos K[200010];
	int biggest=0;
	K[0].iq = 2000000001;
	K[0].ask_i=0;
	int second_n=0;
	int second_term[100011];
	int tmp=0;

	scanf("%d",&T);
	for(int i=0;i<T;++i){
		biggest = 0;
		second_n = 0;
		scanf("%d",&n);
		for(int j = 1; j<=n; ++j){
			scanf("%d",&K[j].iq);
			K[j].ask_i = 0;
			if(K[j].iq > biggest) 
				biggest = K[j].iq;
			find_smarter(K,j);
			if(K[j].ask_i == 0){
				second_n++;			
				second_term[second_n] = j;
			}
		}
		//debug		
		//		for(int a=1;a<=second_n;++a)
		//			printf("%d ",second_term[a]);
		//		printf("\n");
		//debug

		tmp = n;
		for(int j = 1 ; j<=second_n; ++j){
			if(K[second_term[j]].iq != biggest){
//				tmp = n;
				if(K[tmp].iq > K[second_term[j]].iq)
					K[second_term[j]].ask_i = tmp;
				else{
					while(true){
						if(K[K[tmp].ask_i].iq <= K[second_term[j]].iq)
							tmp = K[tmp].ask_i;
						else{
							K[second_term[j]].ask_i = K[tmp].ask_i;
							break;
						}
					}
				}
			}
		}

			for(int j = 1; j<n ; ++j)
				printf("%d ",K[j].ask_i);
			printf("%d\n",K[n].ask_i);
	}
	
	return 0;
}
