#include<cstdio>
#include<cstdlib>
#include<bitset>
#include<cstring>
#include<iostream>
#include<cassert>

using namespace std;

int calltime=0;

struct Degree
{
	int vertex;
	int degree;
};

int compare(const void* a, const void* b)
{
	return((Degree*)a)->degree - ((Degree*)b)->degree;
}

int popcount128(__uint128_t* a)
{
	int p_number=0;
	unsigned long long tmp1,tmp2;

	memcpy(&tmp1,a,8);
	memcpy(&tmp2,a+8,8);
	p_number = __builtin_popcountll(tmp1);
	p_number += __builtin_popcountll(tmp2);
	return p_number;
}


//print out P.R.X status for debugging 
void ccyou(int n, __uint128_t P,__uint128_t R,__uint128_t X,int& max)
{
	__uint128_t one=1;
	printf("get into Bron: ");
	if(1){
	for(int k=0;k<n;++k)
		if(R&(one<<k))
			printf("%2d ",k);
	printf(" , ");
	for(int k=0;k<n;++k){
		if(P&(one<<k))
			printf("%2d ",k);
	}
	printf(" , ");
	for(int k=0;k<n;++k){
		if(X&(one<<k))
			printf("%2d ",k);
	}}
	printf("\n");

	return;
}


void Bron(__uint128_t *vtov,const int& n,__uint128_t& P,__uint128_t& R,__uint128_t& X,int& max,int r_number)
{
	//	ccyou(n,P,R,X,max);
	//	calltime++;
	__uint128_t tmp=0;
	int r_n=0,k=0,i=0;
	__uint128_t p=0,r=0,x=0;
	__uint128_t one=1;
	int p_number=0,p_max=0;
	p_number = popcount128(&P);
	if(max >= (r_number+p_number))//can't be the maximum clique
		return;
	if(!P && !X){//termination
		if(r_number > max)
			max = r_number;
		return;
	}
	else{
		i=0;
		__uint128_t tmp_p = P|X;
		__uint128_t temp_p;
		int sizeu=0;
		for(k=0;k<n;++k){//choose a pivot u in P or X
			if(tmp_p&(one<<k)){
				temp_p = tmp_p&(~vtov[k]);
				sizeu = popcount128(&temp_p);
				if(sizeu > p_max){
					p_max = sizeu;
					i = k;
				}
			}
		}
//		printf("pivot:%d\n",i);
		r_n = r_number+1;
		for(int j=0; j<n;++j){//for each vertex v in P\N(u)
			if(vtov[i]&P&(one<<j)){
				if((((~vtov[i])&P)&((vtov[j])&P)) && j!=i)
					continue;
				if ((vtov[j] & ~vtov[i]) || (j == i))
				r = R|(one<<j);
				//				r_n = r_number+1;
				//build P&N(v) and X&N(v)
				p = P&(~vtov[j]);
				x = X&(~vtov[j]);
				Bron(vtov,n,p,r,x,max,r_n);
				tmp = one<<j;
				P = P&(~tmp); //P := P\(v)
				--p_number;
				X = X|tmp;//X := XU(v)
				if(max >= (r_n+p_number))
					return;
			}
		}
	}
}

int main()
{
	int T=0,n=0, m =0;
	__uint128_t vtov[100];//1 means disconnect 0 means connect
	int a=0, b=0; 
	int max = 0;
	__uint128_t P,R,X;
	__uint128_t one=1;
//	Degree v_degree[100];
	scanf("%d",&T);
	for(int i=0;i<T;++i){
		for(int p=0; p<100; p++){
			vtov[p]=0;
			vtov[p] = vtov[p]|(one<<p);
		}
		R=0;X=0,max=0,P=0;//init
		scanf("%d %d",&n,&m);
		for(int p=0;p<n;++p){
			P=P|(one<<p);
//			v_degree[p].vertex = p;
//			v_degree[p].degree = 0;
		}
		for(int j = 0; j<m; ++j){
			scanf("%d %d",&a, &b);
			vtov[a] = vtov[a]|(one<<b);
			vtov[b] = vtov[b]|(one<<a);
//			v_degree[a].degree++;
//			v_degree[b].degree++;
		}
//		qsort(v_degree,n,sizeof(Degree),compare);
//		for(int j=0;j<n;++j){
//				printf("aa%d %d\n",v_degree[j].degree,v_degree[j].vertex);
//		}
		Bron(vtov,n,P,R,X,max,0);
		printf("%d\n",max);
//		printf("call:%d\n",calltime);
//		calltime=0;
	}
	return 0;
}
