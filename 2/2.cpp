#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<iostream>

using namespace std;

struct Size   //a structure of boxes includes w, width and height
{
	int w;
	int h;
	long long int number;
	Size& operator=(Size const& copy)
	{
		w = copy.w;
		h = copy.h;
		number = copy.number;
		return *this;
	}
	bool operator>(Size const& copy)
	{
		if(w>copy.w && h>copy.h)
			return true;
		else
			return false;
	}
	bool operator>=(Size const& copy)
	{
		if(w>= copy.w && h>=copy.h)
			return true;
		else
			return false;
	}
	bool operator==(Size const& copy)
	{
		if(w == copy.w && h == copy.h)
			return true;
		else return false;
	}
};

void print_mystruct(Size *my, int n)//for debugging
{
	for(int i=0; i<n; i++){
		printf("%d %d %lld\n",my[i].w, my[i].h, my[i].number);
	}
	return;
}

void wh_swap(Size &Scale)//swap bigger one to h
{
	int tmp;
	if(Scale.h < Scale.w){
		tmp  = Scale.h;
		Scale.h = Scale.w;
		Scale.w = tmp;
	}
	return;
}

int compare_box(const void *i, const void *j)//compare function for qsort
{
	Size *a = (Size*)i;
	Size *b = (Size*)j;
	int tmp = a->h - b->h;
	if(tmp != 0)
		return tmp;
	else
		return(a->w - b->w);
}

void sort_box(Size *Scale)//sort for the base case
{
	Size tmp;
	if(Scale[0] == Scale[1]){
		Scale[1].number += 2;
		return;
	}
	else if(Scale[0].w > Scale[1].w){
		tmp = Scale[0];
		Scale[0] = Scale[1];
		Scale[1] = tmp;
	}
	if(Scale[1] >= Scale[0])
		Scale[1].number++;
	else if(Scale[0] >= Scale[1])
		Scale[0].number++;
	return;
}

void combine_box(Size *Scale, int end, int distance) //combine
{
	int i=0, j = distance;
	int rbt_i=0, lbt_j=0;
	Size tmp[100000];
	Size same={0,0,0};
#ifdef DE
	cout<<"before combine\n";//debug
	print_mystruct(Scale,end);//debug
	cout<<endl;//debug*/
#endif
	int k = 0;
	while( i < distance && j < end){
		if(Scale[i] == Scale[j]){
			if(same == Scale[j])
				same.number += 2;
			else{
				same = Scale[j];
				same.number = 2+i;
			}
			Scale[i].number += lbt_j;
			tmp[k] = Scale[i];
			rbt_i=i+1;
			++k;
			++i;
		}
		else if(Scale[j].w >= Scale[i].w){
			Scale[i].number += lbt_j;
			tmp[k] = Scale[i];
			rbt_i=i+1;
			++k;
			++i;
		}
		else if(Scale[i].w > Scale[j].w){
			if(same == Scale[j])
				Scale[j].number += same.number;

/*		int la = i;
			while(la>=0){
				if(Scale[j].w >= Scale[la-1].w){
					Scale[j].number += la;
					break;
				}
				la--;
			}*/
			else
				Scale[j].number += rbt_i;
			if(Scale[i] >= Scale[j])
				lbt_j = j-distance+1;			
			tmp[k] = Scale[j];
			++j;
			++k;
		}
	}	
	
	if(j == end){
	//	int la;
		for(;i<distance;i++){
		/*	la = distance+lbt_j;
			while(la<end){
				if(Scale[i].h >=Scale[la].h){
					Scale[i].number++;
				}
				la++;
			}*/
			Scale[i].number += lbt_j;
			tmp[k] = Scale[i];
			++k;
		}
	}

	if(i >= distance){
		int la;
		for(; j<end; j++){
			la = i-1;
			if(same == Scale[j]){
				Scale[j].number += same.number;
			}
		/*	else{
				while(la<distance){
					if(Scale[j]>=Scale[]){
						Scale[j].number += la;
						break;
					}
					la++;
				}
				Sclae[j].number += rbt_i;
			}*/
			else
				Scale[j].number += rbt_i;
			tmp[k] = Scale[j];
			++k;	
		}
	}

	for(int m= 0; m<end; m++)
		Scale[m] = tmp[m];
#ifdef DE
	cout<<"after combine\n";//debug
	print_mystruct(Scale, end);//debug
	cout<<"\n";//debug*/
#endif 


	return;
}

void merge_box(Size *Scale, int end)//divide
{
	int distance = end;	
	Size tmp;
	if(distance ==1)
		return;
	else if(distance == 2){
		sort_box(Scale);
		return;
	}
	else{
		merge_box(Scale, distance/2);
//		print_mystruct(Scale,distance/2);//debug
		merge_box(Scale + distance/2, distance-distance/2);
//		print_mystruct(Scale+distance/2,end-end/2);//debug
		combine_box( Scale, end, distance/2);
		return;
	}
}

void print_output(Size *Scale, int end)//print output
{
	long long int output=0;
	for(int i=0; i<end; ++i)
		output +=Scale[i].number;
	printf("%lld\n", output);
}

int main()
{
	int T = 0, n = 0;
	Size Scale[100000];
	scanf("%d",&T);
	for(int i=0; i<T; ++i){
		scanf("%d",&n);
		for(int j=0; j<n; ++j){
			scanf("%d %d",&Scale[j].w,&Scale[j].h);
			Scale[j].number = 0;
			wh_swap(Scale[j]);
		}
//		print_mystruct(Scale[i],n);//debug
		qsort(Scale,n, sizeof(Size),compare_box);
//		cout<<endl;//debug
//		print_mystruct(Scale,n);//debug
//		cout<<endl;//debug
		merge_box(Scale, n);
		print_output(Scale, n);
	}
	return 0;
}
