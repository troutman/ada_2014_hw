#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<list>
#include<vector>
#include<iostream>

using namespace std;

struct Event //structure of events includes start ,end points and its value
{
	int start;
	int end;
	int value;
};

bool compare(pair<double,int> i, pair<double,int> j) // compare function for sort
{
	return (i.first < j.first);
}

void go_through_mines(int e_start,int e_end,vector< pair<double,int> >::iterator its, vector< pair<double,int> >::iterator ite,
											int* players_goal,unsigned long long int* player_gain, list<int>* players_to_mines, int* gold_to_players,int n,
											list<int>& uplayer,int* output,unsigned long long int *delta_v,list<int>& done,list<int>& undone)
	//goes through particular mines to check if the player earns at this events achieves his goal or not
{
	unsigned long long int gain_rate=0;
	for(;its != ite; ++its){
		if(its->second == 0){
			delta_v[gold_to_players[(int)its->first]] += gain_rate;
		}
		else{
			gain_rate += its->second;
		}
	}

	for(list<int>::iterator it=uplayer.begin(); it!=uplayer.end();++it){
		if(player_gain[*it]+delta_v[*it] >= (unsigned long long int)players_goal[*it]){
			if(e_start != e_end)
				done.push_back(*it);
			if(output[*it] <= 0){
				output[*it] = (e_start+e_end)/2+1;
			}
			else if((e_start+e_end)/2+1 < output[*it]){
				output[*it] = (e_start+e_end)/2+1;
			}
		}
		else{
			if(e_start != e_end){
				undone.push_back(*it);
				player_gain[*it] +=delta_v[*it];}//if player still doesn't win, plus the gold he gains at this turn, that can save many time to do recursive
			else if(output[*it]<=0)
				output[*it] = -1;
		}
		delta_v[*it] = 0;
		if(e_start == e_end)
			player_gain[*it] = 0;
	}
	return;
}

void win_or_not(int e_start, int e_end, int* players_goal, list<int>* players_to_mines,int* gold_to_players,int m, int n,
								Event* events,unsigned long long int* player_gain,int* output,list<int>& uplayer,unsigned long long int* delta_v)
	//recursive function. check at the middle of start and end points of events to find out if the player meets his goal or not. calls go_through_mines function
{
	if(uplayer.size() == 0)
		return;

	list<int> done;
	list<int> undone;
	vector<pair<double,int> > check;

		for(list<int>::iterator it= uplayer.begin(); it!=uplayer.end(); ++it){
			for(list<int>::iterator itt=players_to_mines[*it].begin(); itt!=players_to_mines[*it].end(); ++itt){
				check.push_back(make_pair((double)(*itt),0));
			}
		}
		for(int i =e_start; i<=(e_start+e_end)/2; ++i){
			check.push_back(make_pair((double)events[i].start-1-0.5,events[i].value));//the flag of event value puts in a vector like a mines
			check.push_back(make_pair((double)events[i].end-0.5,-events[i].value));//but it getsa a 0.5, so it would between two mines or at the edge of vector
		}
	
		sort(check.begin(),check.end(),compare);

		go_through_mines(e_start,e_end,check.begin(),check.end(),players_goal,player_gain, players_to_mines, gold_to_players,n,uplayer,output,delta_v,done,undone);

		if(e_start == e_end)
			return;

	else{
		win_or_not((e_start+e_end)/2+1,e_end,players_goal,players_to_mines,gold_to_players,m,n,events,player_gain,output,undone,delta_v);
		win_or_not(e_start,(e_start+e_end)/2,players_goal,players_to_mines,gold_to_players,m,n,events,player_gain,output,done,delta_v);
	}
	return;
}

int main()
{
	int T=0, m=0, n=0, q=0;//T test cases, number of player n, gold block m, events q
	int g=0, v=0;
	int tmp=0, start=0, end=0;
	int players_goal[100000]={0};//stores each players goal
	list<int> players_to_mines[100000];//use a list for each player to stores his mines
	int gold_to_players[100000]={0};//stores the gold mines is whose
	unsigned long long int player_gain[100000]={0};//stores player's gained money after event
	int output[100000]={0};//the answer
	unsigned long long int delta_v[100000]={0};//stores player's gained money during each event. like a tmp buffer
	Event events[100000];//stores events data
	list<int> uplayer;//stores the player list needs to find out when he wins

	scanf("%d",&T);
	for(int i=0; i<T; ++i){
		scanf("%d %d %d",&n, &m, &q);
		for(int j=0; j<n ; ++j){
			scanf("%d",&players_goal[j]);
			uplayer.push_back(j);
		}
		for(int k=0; k<m; ++k){
			scanf("%d",&tmp);
			gold_to_players[k] = tmp-1;
			players_to_mines[tmp-1].push_back(k);
		}
		for(int p=0; p<q; ++p){
			scanf("%d %d %d", &events[p].start, &events[p].end, &events[p].value);
		}

		win_or_not(0,q-1,players_goal,players_to_mines,gold_to_players,m,n,events,player_gain,output,uplayer,delta_v);
	
		int la=0;
		for( la=0; la<n-1; ++la){
			printf("%d ",output[la]);
			output[la] = 0;
			players_to_mines[la].clear();
		}
		printf("%d\n",output[la]);
		output[la] = 0;
		players_to_mines[la].clear();
		uplayer.clear();
	}
	return 0;
}
