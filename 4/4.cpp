#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>

using namespace std;

struct Memo
{
	int value;
};

Memo table[2002][2002];
//build a table of LCS
int LCS(char* a, char* b,int a_len, int b_len)
{
	for(int k=1; k<=a_len;++k){//let array[k][0] and array[0][k] all become value 0
		table[k][0].value = 0;
	}
	for(int k=1; k<=b_len;++k){
		table[0][k].value = 0;
	}
	for(int i=1; i<=a_len; ++i){
		for(int j=1; j<=b_len;++j){
			if(a[i] == b[j]){
				table[i][j].value = table[i-1][j-1].value+1;
			}
			else{
				if(table[i-1][j].value > table[i][j-1].value){ //up
					table[i][j].value = table[i-1][j].value;
				}
				else{
					table[i][j].value = table[i][j-1].value;//left
				}
			}
		}
	}
	
	return table[a_len][b_len].value;
}

//build a table of string a. It suggests the last position each letter appears at n-th position of string a.
void build_index(char *a,int a_len,int (*a_index)[26])
{
	int i;
	for(i=0;i<26;i++)
			a_index[0][i] = 0;
	a_index[0][a[1]-'a'] = 1;
		
	for(i=1; i<a_len; i++){
		for(int j=0; j<26; ++j){
				if(j != a[i+1]-'a'){
					a_index[i][j] = a_index[i-1][j];
				}
				else
					a_index[i][j] = i+1;
		}
	}

	
	return;
}

void output(int lcs , int a_len, int b_len, int (*a_index)[26], int (*b_index)[26])
{
	int i;
	if(lcs == 0)
		return;
	for(i=0; i<26; ++i){//run a loop from a to z, to check out whether it is the optimal sequence at the specific position.
		if(table[ a_index[a_len-1][i] ][ b_index[b_len-1][i] ].value == lcs){
			printf("%c",i+'a');
			break;
		}
	}
	output(lcs-1,a_index[a_len-1][i]-1,b_index[b_len-1][i]-1,a_index,b_index);
	return;
}

int main()
{
	int T=0;
	char a[2002]={0};//store input a
	char b[2002]={0};//store input b
	char rev_a[2002]={0};//store reverse a
	char rev_b[2002]={0};//store reverse b
	int a_len=0, b_len=0;
	int a_index[2002][26]={0};//the last position of each letters appears at n-th char of string
	int b_index[2002][26]={0};
	int k;
	int lcs=0;

	scanf("%d", &T);
	for(int i=0;i<T;++i){
		scanf("%s %s",a+1, b+1);
		a_len = strlen(a+1);
		b_len = strlen(b+1);
		k=0;
		for(int p=a_len;p>=0;--p ,k++)
			rev_a[k+1] = a[p];
		k=0;
		for(int p=b_len;p>=0; --p, k++)
			rev_b[k+1] = b[p];
		lcs = LCS(rev_a,rev_b, a_len, b_len);
		build_index(rev_a,a_len,a_index);
		build_index(rev_b,b_len,b_index);
		output(lcs,a_len, b_len, a_index, b_index);
		cout<<endl;
	}
	return 0;
}

