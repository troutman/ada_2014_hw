#include<bitset>
#include<cstdlib>
#include<cstdio>
#include<algorithm>


#define mod_n 1000000007
int table[17][17][65536]={0};


//put X around the given block
void init_block(int n, int m, char (*board)[17])
{
	int i, j, k;
	unsigned short tmp=1;
	for(int i=0;i<=n;++i)
		board[i][m] = 'X';
	for(int i=0; i<=m;++i)
		board[n][i] = 'X';
	return;
}

//doing DP
void DP_each_block(int n, int m,char (*board)[17]/*,int (*table)[16][65536]*/)
{
	unsigned short possible = 1<<m;//how many possible cases
	unsigned short remainder=0, quotient=0;//trash
	unsigned short tmp=1,tmp1=1,tmpi;//for bir operation
	table[0][0][0] = 1;//initialization the table[][][]
	for(int now_n=0; now_n<n; ++now_n){
		for(int now_m=0;now_m<m;++now_m){
			tmp = (1<<now_m);//000001->000100
			tmp1 = (1<<(now_m+1));//000001->001000
			if(board[now_n][now_m] =='X')//if this block is a X, transmit each the sum of each cases to next block
				for(unsigned short int i=0; i<possible; ++i){
					table[now_n+(now_m+1)/m][(now_m+1)%m][i] += table[now_n][now_m][i];					
					table[now_n+(now_m+1)/m][(now_m+1)%m][i] %= mod_n;
					table[now_n][now_m][i]=0;//vacate the value
				}
			else{
				for(unsigned short int i=0; i<possible; ++i){
					if(table[now_n][now_m][i] != 0){//if there is a value to transmit
						tmpi = ~i;
						if(i & tmp){//this block is occupied
							table[now_n+(now_m+1)/m][(now_m+1)%m][i&(~tmp)] += table[now_n][now_m][i];
							table[now_n+(now_m+1)/m][(now_m+1)%m][i&(~tmp)] %= mod_n;
						}
						else{ // this block is free
							if(board[now_n][now_m+1] != 'X'){
								if(tmpi & tmp1){//put a 1*2 block
									table[now_n+(now_m+2)/m][(now_m+2)%m][i] += table[now_n][now_m][i];	
									table[now_n+(now_m+2)/m][(now_m+2)%m][i] %= mod_n;	
								}
							}
							if(board[now_n+1][now_m] != 'X'){//put 2*1 block
								table[now_n+(now_m+1)/m][(now_m+1)%m][i|tmp] += table[now_n][now_m][i];
								table[now_n+(now_m+1)/m][(now_m+1)%m][i|tmp] %= mod_n;
							}
							if(board[now_n+1][now_m] != 'X' && board[now_n][now_m+1] != 'X' && board[now_n+1][now_m+1]!='X'){
								if(tmpi & tmp1){//put 2*2 block
									table[now_n+(now_m+2)/m][(now_m+2)%m][i|(tmp|tmp1)] += table[now_n][now_m][i];
									table[now_n+(now_m+2)/m][(now_m+2)%m][i|(tmp|tmp1)] %= mod_n;
								}
							}//put 1*1
							table[now_n+(now_m+1)/m][(now_m+1)%m][i] += table[now_n][now_m][i];
							table[now_n+(now_m+1)/m][(now_m+1)%m][i] %= mod_n;
						}
						table[now_n][now_m][i] = 0;
					}
				}
			}
		}
	}

	printf("%d\n",table[n][0][0]%1000000007);
	table[n][0][0]=0;
	return;
}

using namespace std;

int main()
{
	int T=0;
	int n=0, m=0;
	char board[17][17]={0};

	scanf("%d",&T);
	for(int i=0; i<T; ++i){
		scanf("%d %d",&n, &m);
		for(int j=0; j<n; ++j){
			scanf("%s",board[j]);
		}
		init_block(n,m,board);

		DP_each_block(n,m,board/*,table*/);
	}

	return 0;
}
