#include<cstdio>
#include<cstdlib>
#include<algorithm>

#define maximum 1000000002

using namespace std;

int compare(const void*a, const void*b)
{
	return(*(int*)a - *(int*)b);
}

int interval_number(int max_interval_width,int n,int p,int *dis)
{
	int count = 1,s=1;
	bool flag = false;
//	printf("getin\n");
	for(int i=1; i<=n; ++i){
//		printf("before i:%d s:%d   after",i,s);
		if(dis[i] - dis[s] > max_interval_width){
			if(flag == false){
				s = i-1;
				i--;
				flag = true;
			}
			else{
				s = i;
				count++;
//				printf(" count:%d   ",count);
				flag = false;
			}
			if(count > p)
				break;
		}
//		printf(" i:%d s:%d max:%d\n",i,s,max_interval_width);
	}
	return count;
}

int binary_search(int n, int k, int* dis)
{
	int ans = dis[n];
	int R = dis[n];
	int L =0;
	for(int M = (R+L+1)/2; L <= R ; M= (R+L)/2){
		int p = interval_number(M,n,k,dis);
//		printf("L:%d R:%d\n",L,R);
		if(p<=k){
//			printf("p:%d M:%d\n",p, M);
			if(M<ans)
				ans = M;
			R = M-1;
		}
		else{
			L = M+1;
		}
	}
	return ans;
}

int main()
{
	int T=0,k=0, n=0;
	int dis[100001]={0};
	int output=0;

	scanf("%d",&T);
	for(int i=0; i<T; ++i){
		scanf("%d %d",&n, &k);
		for(int j=1; j<=n; ++j){
			scanf("%d",&dis[j]);
		}
		qsort(dis+1,n,sizeof(int),compare);
		output = binary_search(n,k,dis);
		printf("%d\n",output);
	}
	return 0;
}
