#include<cstdio>
#include<cstdlib>

using namespace std;

int money_sum(int *c, int *di)
{
	int sum=0;
	for(int i=0; i<10;++i){
		sum += di[i]*c[i];
	}
	return sum;
}

void check_money(int*  c, int p, int* di)
{
	int tmp[4]={0};
	int cases[4]={0};//0: even 50 even 500; 1:even 50 odd 500; 2: odd 50 even 500; 3:odd 50 odd 500

	/*	for(int i=0;i<10;++i)
			printf("%d:%d   ",di[i],c[i]);
			printf("\n");*/

	int sum = money_sum(c,di);
	int spend[4][10];
	int flag=0;
	for(int i=0; i<10;++i){
		spend[0][i] = c[i];
		spend[1][i]	= c[i];
		spend[2][i] = c[i];
		spend[3][i] = c[i];
//		printf("spend:%d  :%d  sum:%d\n",di[i],spend[0][i],sum);
	}


	p -= sum;
	if(p>0){
		printf("-1\n");
		return;
	}
	else{
		int more[4] = {-p,-p,-p,-p};
//				printf("more:%d\n",more[2]);
/*				for(int i=0;i<10;++i)
					printf("%d:%d   ",di[i],spend[0][i]);
					printf("\n");*/
		if(more[3] >= 550 && spend[3][7]>0 && spend[3][4]>0){
			more[3] -= 550;
			spend[3][4] -=1;
			spend[3][7] -=1;
		}
		if(more[1] >= 500 && spend[1][7]>0){
			more[1] -= 500;
			spend[1][7] -=1;
		}
		if(more[2]>=50 && spend[2][4]>0){
			more[2] -= 50;
			spend[2][4] -= 1;
		}


		for(int k=0;k<4;++k){
			for(int i=9;i>=0;i--){
				tmp[k] = more[k]/di[i];
				if(i == 7 || i==4){
//					printf("no here\n");
					flag = tmp[k]%2;
					if(spend[k][i]>=2){
						if(tmp[k]>=spend[k][i]){
							more[k] -= di[i]*(spend[k][i]-spend[k][i]%2);
							spend[k][i] = spend[k][i]%2;
						}
						else{
							more[k] -= di[i]*(tmp[k]-flag);
							spend[k][i] -= (tmp[k]-flag);
						}
					}
				}

				else{
					if(tmp[k]>=spend[k][i]){
						more[k] -= di[i]*spend[k][i];
						spend[k][i] = 0;
//						printf("more:%d spend:%d\n",more[k],spend[k][i]);
					}
					else{
						more[k] -= di[i]*tmp[k];
						spend[k][i] -= tmp[k];
//						printf("more:%d spend:%d *%d\n",more[k],di[i],spend[k][i]);
					}
				}
			}
		}


		if(more[0]!=0 && more[1]!=0 && more[2]!=0 && more[3]!=0){
			printf("-1\n");
			return;
		}
		else{
			int output[4]={0};
			for(int k=0; k<4;++k){
				if(more[k]==0)
					for(int i=0;i<10;++i){
						output[k] += spend[k][i];
			//			printf("%d:%d + ",di[i],spend[k][i]);
					}
				else
					output[k] = -1;
//				printf("outpute:%d \n",output[k]);
			}

			for(int i=1;i<4;++i)
				if(output[i]<output[i-1])
					output[i] = output[i-1];
			printf("%d\n",output[3]);
		}
	}


	return;
}

int main()
{
	int T=0,p=0;
	int di[10]={1,5,10,20,50,100,200,500,1000,2000};//50:4   500:7
	int  c[10];
	scanf("%d",&T);
	for(int i=0;i<T;++i){
		scanf("%d",&p);
		for(int j=0;j<10;++j)
			scanf("%lld",&c[j]);
		check_money(c,p,di);
	}
	return 0;
}
