#include<cstdio>
#include<cstdlib>
#include<queue>

#define MAXSTEP ~(1<<31)

using namespace std;

struct Room
{
	int visit;//0=haven't visited, 1=visited, -1=in the queue
	char c;//what is this block
	int step;//count step
	int B;//count beverage
	void operator=(const Room& b){
		visit = b.visit;
		step = b.step;
		B = b.B;
		c = b.c;
	}
};

struct Room_p
{
	int x;
	int y;
	int z;
};

Room room[103][103][103];

void run_bfs(int n, Room_p start, Room_p end)
{
	queue<Room_p> list;//store the queue
	room[start.x][start.y][start.z].visit = 1;
	room[start.x][start.y][start.z].step = 0;
	list.push(start);
	Room_p tmp;
	Room_p tmp2;
	int x=0,y=0,z=0;
	int isB=0;

	while(!list.empty()){
		tmp = list.front();
		if(room[tmp.x][tmp.y][tmp.z].step >= room[end.x][end.y][end.z].step)
			break;
//		fprintf(stderr,"f:%d %d %d\n",tmp.x,tmp.y, tmp.z);
		list.pop();
		for(int i=1;i<=6;++i){//1:x+1; 2:x-1  3: y+1; 4:y-1; 5:z+1; 6:z-1
			isB=0;
			switch(i){
				case 1: x = tmp.x+1; y = tmp.y; z = tmp.z; break;
				case 2: x = tmp.x-1; y = tmp.y; z = tmp.z; break;
				case 3: x = tmp.x; y = tmp.y+1; z = tmp.z; break;
				case 4: x = tmp.x; y = tmp.y-1; z = tmp.z; break;
				case 5: x = tmp.x; y = tmp.y; z = tmp.z+1; break;
				case 6: x = tmp.x; y = tmp.y; z = tmp.z-1; break;
			}
			if(x<1 || y<1 || z<1 || x > n || y>n || z>n)
				continue;
			if(room[x][y][z].c == '#')
				continue;
			if(room[x][y][z].visit == 0){
				tmp2.x = x; tmp2.y = y; tmp2.z = z;
				list.push(tmp2);
				room[x][y][z].visit = 1;
			}

			isB = (room[x][y][z].c == 'B')? 1:0;

			if(room[x][y][z].step < room[tmp.x][tmp.y][tmp.z].step+1)
				continue;

			if(room[tmp.x][tmp.y][tmp.z].step+1 < room[x][y][z].step){
				room[x][y][z].step = room[tmp.x][tmp.y][tmp.z].step+1;
				room[x][y][z].B = room[tmp.x][tmp.y][tmp.z].B + isB;
			}
			else{
				if(room[tmp.x][tmp.y][tmp.z].B + isB > room[x][y][z].B){
					room[x][y][z].B = room[tmp.x][tmp.y][tmp.z].B + isB;
				}
			}	
			
		}
	}
	while(!list.empty())
		list.pop();

	if(room[end.x][end.y][end.z].visit == 0)
		printf("Fail OAQ\n");
	else
		printf("%d %d\n",room[end.x][end.y][end.z].step,room[end.x][end.y][end.z].B);
	return;
}

int main()
{
	int i, j, k;
	int T;
	int n;
	char buf[110];
	Room_p s;//start from x y z
	Room_p e;
	scanf("%d",&T);
	for(i=0;i<T;++i){
		scanf("%d",&n);
		for(int x=1;x<=n;++x){
			for(int y=1;y<=n;++y){
				scanf("%s",buf);
				for(int z=1;z<=n;++z){
					room[x][y][z].c = buf[z-1];
					room[x][y][z].step = MAXSTEP;
					room[x][y][z].visit = 0;
					room[x][y][z].B = 0;
					if(room[x][y][z].c=='S'){//where S located
						s.x = x;	s.y = y;	s.z = z;
					}
					else if(room[x][y][z].c == 'E'){
						e.x = x; e.y = y; e.z = z;
					}
				}
			}
		}
		run_bfs(n,s,e);
	}
	return 0;
}
