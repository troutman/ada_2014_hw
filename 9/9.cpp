#include<cstdlib>
#include<cstdio>
#include<utility>
#include<vector>
#include<list>
#include<cstring>

#define shift(n) (1<<n)
#define isone(n,tmp) (n & tmp)

using namespace std;
struct Edge
{
	int e1;
	int e2;
	int w;
};

int find(int x , int *p)
{
	return x==p[x]? x:p[x] = find(p[x],p);
}

void cool_cost(list<Edge>& edges,int n, int m,int *disjoint)
{
	int disjoint0[100001];
	int tmp;
	int output = 0;
	int size0=0,size1=0;
	int group = n;
	int flag=0;
	for(int i=30;i>=0; --i){
		memcpy(disjoint0,disjoint,n*sizeof(int));
		disjoint0[n] = n;
		flag = 0;
		tmp = shift(i);
		group = n;
		for(list<Edge>::iterator it= edges.begin(); it!=edges.end() && !edges.empty(); ++it){
			//			printf("???%d size:%d\n",!(it->w & tmp), edges.size());
			if(!isone(it->w,tmp)){
				//				fprintf(stderr,"it %d tmp:%d \n",it->w,tmp);
				if(find(it->e2,disjoint0) != find(it->e1,disjoint0)){
					group--;
					//	printf("group:%d\n",group);
					disjoint0[find(it->e2,disjoint0)] = find(it->e1,disjoint0);
				}

			}
		}
		if(group == 1){
			for(list<Edge>::iterator it= edges.begin(); it!=edges.end(); ){
				if(isone(it->w,tmp))
					it = edges.erase(it);
				else
					++it;
			}	
		}
		else{
			output = output | tmp;
		}
	}
	printf("%d\n",output);
	return;
}

int main()
{
	int T, n, m,a,b;
	list< Edge > edges;
	Edge buf;
	int disjoint[100001]={0};
	for(int i=0; i<100001;++i)
		disjoint[i] = i;

	scanf("%d",&T);
	for(int i=0; i<T;++i){
		while(!edges.empty())
			edges.pop_front();
		scanf("%d %d",&n, &m);
		for(int j=0;j<m; ++j){
			scanf("%d %d", &a, &b);
			buf.e1 = a;
			buf.e2 = b;
			scanf("%d",&buf.w);
			edges.push_back(buf);
		}
		cool_cost(edges,n,m,disjoint);
	}
	return 0;
}
